import React from 'react'
import './footer.css'
import logo from "../../assets/imgs/logo.png";
import SocialList from "../socialList/socialList";

const Footer = (props) => {
    return (
        <footer className="footer">
            <div className="container">
                <div className="logo">
                    <img src={logo} alt=""/>
                </div>
                <div className="politics" onClick={props.openModal}>
                    <div className="link">Политика обработки персональных данных</div>
                </div>
                <SocialList />
            </div>
        </footer>
    )
}

export default Footer

import React from 'react'
import logo from '../../assets/imgs/logo.png'
import './header.css'
import Button from "../ui/button/button";
import './header.css'
import {NavLink, withRouter, useLocation } from "react-router-dom";

const Header = (props) => {
  const location = useLocation()
    return (
        <header className="header">
            <div className="container">
              <NavLink to='/' className='link__navlink'>
                <div className="logo">
                  <img src={logo} alt=""/>
                </div>
              </NavLink>
              {console.log(location.pathname)}
              {
                location.pathname === '/' &&
                <NavLink to='/subscribe' className='link__navlink'>
                  <Button type='btn-fix-width' >
                    Подключить подписку
                  </Button>
                </NavLink>
              }
            </div>
        </header>
    )
}

export default withRouter(Header)

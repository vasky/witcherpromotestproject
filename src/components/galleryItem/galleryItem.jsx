import React from 'react'
import gallery1 from "../../assets/imgs/gallery/1.png";
import gallery3 from "../../assets/imgs/gallery/3.png";
import gallery2 from "../../assets/imgs/gallery/2.png";
import gallery4 from "../../assets/imgs/gallery/4.png";
import gallery5 from "../../assets/imgs/gallery/5.png";
import gallery6 from "../../assets/imgs/gallery/6.png";
import gallery7 from "../../assets/imgs/gallery/7.png";
import gallery8 from "../../assets/imgs/gallery/8.png";
import './galleryItem.css'
import classNames from "classnames";

export default function GalleryItem(props){

  const cls = classNames({
    invert: props.invert || false
  })
  const clsRightPart = classNames({
    'only-desktop': !props.showMorePhotos || false
  })
  return(

    <div className={"gallery__item " + cls}>
      <div className="gallery__item-left">
        <div className="gc__item__photo gallery__item--1">
          <img src={gallery1} alt=""/>
        </div>
        <div className="gc__item__photo gallery__item--2">
          <img src={gallery3} alt=""/>
        </div>
        <div className="gc__item__photo gallery__item--3">
          <img src={gallery2} alt=""/>
        </div>
        <div className="gc__item__photo gallery__item--4">
          <img src={gallery4} alt=""/>
        </div>
      </div>
      <div className={"gallery__item-right " + clsRightPart}>
        <div className="gc__item__photo gallery__item--5">
          <img src={gallery5} alt=""/>
        </div>
        <div className="gc__item__photo gallery__item--6">
          <img src={gallery6} alt=""/>
        </div>
        <div className="gc__item__photo gallery__item--7">
          <img src={gallery7} alt=""/>
        </div>
        <div className="gc__item__photo gallery__item--8">
          <img src={gallery8} alt=""/>
        </div>
      </div>
    </div>
  )
}

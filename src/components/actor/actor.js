import React from 'react'
import './actor.css'

export default function Actor(props){
  return (
    <div
      style={{backgroundImage: `url('${props.photo}')`}}
      className='actor-card'>
      <div className="actor__content">
        <div className="ac__title-part">
          <div className="actor__hero">
            <h4 className="b">{props.hero}</h4>
          </div>

          <div className="actor__hero">
            <h4>{props.name}</h4>
          </div>
        </div>
        <div className="actor__description"><p>{props.description}</p></div>
      </div>
      <div className="bg-black"></div>
    </div>
  )
}

import React from 'react'
import './socialList.css'
import insta from './../../assets/imgs/socials/insta.png'
import facebook from './../../assets/imgs/socials/facebook.png'
import vk from './../../assets/imgs/socials/vk.png'

export default function SocialList(props) {
  return(
    <div className='social-list'>
      <ul className="social-list__ul">
        <li className="social-list__item">
          <div
            style={{WebkitMaskImage: `url('${insta}')`}}
            className="sli__icon" ></div>
          <div className="sli__substrate"></div>
        </li>
        <li className="social-list__item">
          <div
            style={{WebkitMaskImage: `url('${facebook}')`}}
            className="sli__icon" ></div>
          <div className="sli__substrate"></div>
        </li>
        <li className="social-list__item">
          <div
            style={{WebkitMaskImage: `url('${vk}')`}}
            className="sli__icon" ></div>
          <div className="sli__substrate"></div>
        </li>
      </ul>
    </div>
  )
}

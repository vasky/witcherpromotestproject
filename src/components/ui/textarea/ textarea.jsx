import React, {useState} from 'react'
import './textarea.css'
import classNames from "classnames";

export default function Textarea(props) {
  const [validated, setValidated] = useState(false)
  const [touched, setTouched] = useState(false)

  const handleChange = (e) => {
    setTouched(true)
    const valueTextLength = e.target.value.length
    const result = valueTextLength > 10 && valueTextLength < 200
    setValidated(result)
    if(props.updateValid){
      props.updateValid(props.indexInForm, result)
    }
  }

  const cls = classNames({
    fail: touched && !validated || props.formSendClick ? !validated : null
  })
  return(
    <div className={'textarea__wrapper ' + cls}>
      <textarea
        onChange={handleChange}
        className='textarea '
        placeholder={props.placeholder} >
      </textarea>
      <div className="error-message">
        {props.errorMessage}
      </div>
    </div>
  )
}

import React, {useState} from 'react'
import './select.css'
import arrow from '../../../assets/imgs/arrowSelect.svg'
import classNames from "classnames";

export default function Select({items = [], title, errorMessage = 'Элемент не выбран', updateValid = null, index = 0, formSendClick = null}) {
  const [open, setOpen] = useState(false)
  const [activeItem, setActiveItem] = useState(title || 'Выберите элемент из списка')
  const [validated, setValidated] = useState(false)
  const [touched, setTouched] = useState(false)


  const cls = classNames({
    open,
    fail: touched && !validated && !open || formSendClick ? !validated : null
  })

  const toggle = ()=> {
    setOpen(!open)
    setTouched(true)
  }
  const onItemClick = (item) => {
    setActiveItem(item)
    setOpen(false)
    setValidated(true)
    if(updateValid) {
      updateValid(index, true )
    }
  }

  return(
    <div
      className={'select__wrapper ' +  cls}
      onClick={toggle}
    >
      <div className="select__header">
        <div className="sh__title">
          {activeItem}
        </div>
        <div className="sh__arrow">
          <img src={arrow} alt=""/>
        </div>
      </div>
      <div className="select__body">
        { open && (
          <ul className="select__ul">
            {
              items.map(el=>{
                return (<li onClick={()=>onItemClick(el)} key={el + Math.random()}>{el}</li>)
              })
            }
          </ul>
        )}
      </div>
      <div className="error-message">
        {errorMessage}
      </div>
    </div>
  )
}

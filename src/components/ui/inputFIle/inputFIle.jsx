import React, {useRef, useState} from 'react'
import './inputFIle.css'
import clipIcon from './../../../assets/imgs/iconmonstr-paperclip-1 1.svg'
import classNames from "classnames";

export default function InputFile(props) {
  const inputBody = useRef()
  const [inputName, setInputName] = useState('Прикрепите файл')
  const [validated, setValidated] = useState(false)
  const [touched, setTouched] = useState(false)

  function onInputChange(e) {
    setInputName(e.target.files[0] ? e.target.files[0].name : 'Прикрепите файл' )
    setTouched(true)
    const result = e.target.files[0] ? true : false
    setValidated(result)
    if(props.updateValid){
      props.updateValid(props.indexInForm, result )
    }
  }

  const cls = classNames({
    fail: touched && !validated || props.formSendClick ? !validated : null
  })

  return (
    <div className={"input-file " + cls} onClick={()=>inputBody.current.click()}>
      <input
        onChange={onInputChange}
        ref={inputBody}
        type="file"
        className='hide'/>
      <div className="input-file__name">
        {inputName}
      </div>
      <img src={clipIcon} alt=""/>
      <div className="error-message">
        {props.errorMessage}
      </div>
    </div>
  )
}

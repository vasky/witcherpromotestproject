import React, {useRef, useState} from 'react'
import './checkbox.css'
import classNames from "classnames";

export default function Checkbox(props) {
  const inputBody = useRef()
  const [validated, setValidated] = useState(false)
  const [touched, setTouched] = useState(false)

  const cls = classNames({
    fail: touched && !validated || props.formSendClick ? !validated : null
  })

  const handleChange = (e) => {
    setTouched(true)
    const value = !inputBody.current.checked
    inputBody.current.checked = value
    setValidated(value)
    if(props.updateValid) {
      props.updateValid(props.indexInForm, value)
    }
  }
  return(
    <div
      className={"checkbox__wrapper " + cls}
      onClick={handleChange}
      >
      <input
        onChange={handleChange}
        ref={inputBody}
        type="checkbox"
        className="custom-checkbox hide"
        id="custom-checkbox"
        name="happy" />
      <label htmlFor="happy">{props.caption}</label>
      <div className="error-message">
        {props.errorMessage}
      </div>
    </div>
  )
}

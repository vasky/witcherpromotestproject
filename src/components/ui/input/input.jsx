import React, {useState} from 'react'
import './input.css'
import classNames from "classnames";
import PropTypes from 'prop-types'
import is from 'is_js'
import InputMask from 'react-input-mask';

function Input(props){
  const [validated, setValidated] = useState(false)
  const [touched, setTouched] = useState(false)
  const cls = classNames({
    fail: touched && !validated || props.formSendClick ? !validated : null
  })

  const handleInput = (event) => {
    const valueInput = event.target.value;
    switch (props.inputType){
      case "name":
        const result = valueInput.length>=4 && valueInput.length <= 30
        setValidated(result)
        localUpdateValid(result)
        break;
      case "email":
        const result1 = is.email(valueInput)
        setValidated(result1)
        localUpdateValid(result1)
        break;
      case "phone":
        if(valueInput.match(/_/g)){
          const result = valueInput.match(/_/g).length <= 1
          setValidated(result)
          localUpdateValid(result)
        }else{
          setValidated(false)
          localUpdateValid(false)
        }
        break;
    }
  }

  const localUpdateValid = (value) => {
    if(props.updateValid){
      console.log(value)
      props.updateValid(props.indexInForm, value)
    }
  }
  const drawRightInput = () => {
    if(props.inputType !== 'phone') {
      return (
        <input
          onInput={handleInput}
          type={props.type} className='input__field  ' placeholder=' '/>
      )
    }else{
      return(
        <InputMask
          onInput={handleInput}
          type="text"
          class="input__field
          " placeholder=" "
          mask="+7(999) 999-99-99">

        </InputMask>
      )
    }
  }
  return(
    <div className={'input__wrapper ' + cls}
      onClick={()=>setTouched(true)}
    >
      <label className='input'>
        { drawRightInput() }
        <span className='input__label  '>{props.label}</span>
      </label>
      <div className="error-message">
        {props.errorMessage}
      </div>
    </div>
  )
}

Input.propTypes = {
  inputType: PropTypes.oneOf(['name', 'email', 'phone'])
}

export default Input

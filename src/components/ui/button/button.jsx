import React from 'react'
import './button.css'


export default function Button(props) {
  const cls = props.type;
  return (
    <>
    <button
      onClick={props.onClick || null}
      className={"btn  " + cls}>{props.children}</button>
    </>
  )
}

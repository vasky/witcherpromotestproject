import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import './fonts/FuturaPT-Book.ttf'
import './fonts/FuturaPT-Demi.ttf'
import './fonts/FuturaPT-Heavy.ttf'
import './fonts/FuturaPT-Medium.ttf'
import './fonts/OrchideaPro-Medium.ttf'
import './fonts/OrchideaPro-SemiBold.ttf'
import {BrowserRouter} from "react-router-dom";

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

import React, {useState} from 'react'
import './App.css';
import MainPage from "./pages/mainPage/mainPage";
import Header from "./components/header/header";
import Footer from "./components/footer/footer";
import Modal from "./components/modal/modal";
import { Route } from 'react-router-dom'
import SubscribePage from "./pages/subscribePage/subscribePage";

function App() {
  const [modalStatus, setModalStatus] = useState(false)
  const closeModal = () => {
    setModalStatus(false)
  }
  const openModal = () => {
    setModalStatus(true)
  }
  return (
    <div className="App">
      <Modal closeModal={closeModal} modalStatus={modalStatus} />
      <Header />
      <Route path='/' exact  >
        <MainPage />
      </Route>
      <Route path='/subscribe' exact  >
        <SubscribePage/>
      </Route>
      <Footer openModal={openModal} />
    </div>
  );
}

export default App;

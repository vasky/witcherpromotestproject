import React, {useState} from 'react'
import Button from "../../components/ui/button/button";
import './mainPage.css'
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.min.css'
import Actor from "../../components/actor/actor";
import geraltPhoto from './../../assets/imgs/actors/1.jpg'
import SwiperCore, {Pagination, Navigation} from "swiper";
import GalleryItem from "../../components/galleryItem/galleryItem";
import { MapContainer, TileLayer, Marker } from 'react-leaflet'
import {Icon} from "leaflet";
import mapIconSvg from './../../assets/imgs/mapIcon.png'
import {NavLink} from "react-router-dom";

SwiperCore.use([Pagination, Navigation])

const mapIcon = new Icon({
  iconUrl: mapIconSvg,
  iconSize: [40, 40]
})
export default function MainPage(props) {


    const actors = [
        {
            photo: geraltPhoto,
            hero: 'Геральт',
            name: 'Генри Кавилл',
            description: 'Один из центральных персонажей сериала, лучший друг и неизменный спутник Геральта, трубадур и бабник'
        },
        {
            photo: geraltPhoto,
            hero: 'Геральт',
            name: 'Генри Кавилл',
            description: 'Один из центральных персонажей сериала, лучший друг и неизменный спутник Геральта, трубадур и бабник'
        },
        {
            photo: geraltPhoto,
            hero: 'Геральт',
            name: 'Генри Кавилл',
            description: 'Один из центральных персонажей сериала, лучший друг и неизменный спутник Геральта, трубадур и бабник'
        },
        {
            photo: geraltPhoto,
            hero: 'Геральт',
            name: 'Генри Кавилл',
            description: 'Один из центральных персонажей сериала, лучший друг и неизменный спутник Геральта, трубадур и бабник'
        },
        {
            photo: geraltPhoto,
            hero: 'Геральт',
            name: 'Генри Кавилл',
            description: 'Один из центральных персонажей сериала, лучший друг и неизменный спутник Геральта, трубадур и бабник'
        },
        {
            photo: geraltPhoto,
            hero: 'Геральт',
            name: 'Генри Кавилл',
            description: 'Один из центральных персонажей сериала, лучший друг и неизменный спутник Геральта, трубадур и бабник'
        }
    ]

    const [showMorePhotos, setShowMorePhotos] = useState(false)
    const showMorePhotosFunc = () => {
      showMorePhotos ? setShowMorePhotos(false) : setShowMorePhotos(true)
    }

    const mapMarkers = [[55.749434, 37.621949], [55.751463, 37.551450], [55.777925, 37.511942]]
    const middleMap = [(mapMarkers[0][0] + mapMarkers[mapMarkers.length-1][0])/2, (mapMarkers[0][1] + mapMarkers[mapMarkers.length-1][1])/2,]
    const zoom = window.innerWidth >= 768 ? 13.3 : 11
    return (
        <main className="main">
            <section className='fb'>
                <div className="container">
                    <div className="fb__content">
                        <div className="title">
                            <h1>Сериал Ведьмак</h1>
                        </div>
                        <p className="fb__description">
                            Геральт из Ривии, наемный охотник за чудовищами, перенесший мутации, идет навстречу своей судьбе
                            в неспокойном мире, где люди часто оказываются
                            куда коварнее чудовищ.
                        </p>
                        <NavLink to='/subscribe'  className='link__navlink' >
                          <Button type='btn-red' >
                              Смотреть сериал
                          </Button>
                        </NavLink>
                    </div>
                </div>
            </section>
            <section className="actors">
                <div className="container">
                    <div className="title-part">
                        <h2>Актерский состав</h2>
                        <div className="slider-control">
                            <div className="arrow arrow-prev"></div>
                            <div className="arrow arrow-next"></div>
                        </div>
                        <div className="slider-actors__progress"></div>
                    </div>

                    <Swiper
                      spaceBetween={24}
                      slidesPerView={'auto'}
                      className='actors-slider'
                      pagination={{
                          el: '.slider-actors__progress',
                          type: "progressbar"}}
                      navigation={{
                          prevEl: ".arrow-prev",
                          nextEl: ".arrow-next",
                      }}
                    >
                        {actors && actors.map(actorItem => {
                            return(
                              <SwiperSlide className='actor-slider'>
                                  <Actor
                                    {...actorItem}
                                  />
                              </SwiperSlide>
                            )
                        })
                        }
                    </Swiper>
                </div>
            </section>
            <section className="gallery">
                <div className="container">
                    <div className="title">
                        <h2>Кадры со съемок</h2>
                    </div>
                    <div className="gallery__content">
                      <GalleryItem showMorePhotos={showMorePhotos} />
                      { showMorePhotos &&
                        <>
                          <GalleryItem invert showMorePhotos={showMorePhotos} />
                          <GalleryItem showMorePhotos={showMorePhotos}  />
                        </>
                      }
                    </div>
                    <Button
                      onClick={showMorePhotosFunc}
                      type='btn-full-width' >
                      { !showMorePhotos ? "Показать еще" : 'Скрыть фотографии' }
                    </Button>
                </div>
            </section>
            <section className="map">
              <div className="container">
                <div className="title">
                  <h2>Магазины мерча ведьмака</h2>
                </div>
              </div>
              <div className="container map_container">
                <div className="map__content">
                  <MapContainer center={ middleMap } zoom={zoom} zoomControl={false}>
                    <TileLayer
                      attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    {mapMarkers && mapMarkers.map(marker=>{
                      return (

                        <Marker
                          icon={mapIcon}
                          position={marker}>
                        </Marker>
                      )
                    })}
                  </MapContainer>
                </div>
              </div>
            </section>
        </main>
    )
}

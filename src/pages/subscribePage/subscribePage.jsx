import React, {useState} from 'react'
import './subscribePage.css'
import Select from "../../components/ui/select/select";
import Input from "../../components/ui/input/input";
import Textarea from "../../components/ui/textarea/ textarea";
import InputFile from "../../components/ui/inputFIle/inputFIle";
import Checkbox from "../../components/ui/checkbox/checkbox";
import Button from "../../components/ui/button/button";
import {NavLink} from "react-router-dom";

export default function SubscribePage(props){
  const selectItems = [
    'Москва', 'Санкт-Питербург', 'Казань', 'Краснодар', 'Ростов на дону'
  ]
  const [btnPressed, setBtnPressed] = useState(false)
  const [validForm, setValidForm] = useState(false)
  const formElements = [
    {name: 'select', validated: false},
    {name: 'input name', validated: false},
    {name: 'input email', validated: false},
    {name: 'input phone', validated: false},
    {name: 'textarea', validated: false},
    {name: 'input file', validated: false},
    {name: 'checkbox', validated: false},
  ]
  const updateValidateFormElements = (index, validated) => {
    formElements[index].validated = validated
    console.log(formElements)
  }
  const handleClickSend = () => {
    setBtnPressed(true)
    setValidForm(validateForm())
  }
  const validateForm = () => {
    let result = true
    formElements.forEach(el => {
      if(!el.validated){
        result = false
      }
    })
    return result
  }

  return(
    <div className='subscribe-page'>
      <div className="container">


        <div className="title">
          { !validForm && <h1>Оставьте заявку</h1>}
          { validForm && <h1>Заявка <br /> отправлена!</h1>}
        </div>
        <div className="subscribe__content">
          {!validForm &&
          <div className="subscribe__form-part">

            <Select formSendClick={btnPressed} updateValid={updateValidateFormElements} indexInForm={0} items={selectItems} title='Выберете город' errorMessage='Город не выбран' />
            <Input formSendClick={btnPressed} updateValid={updateValidateFormElements} indexInForm={1} type='text' inputType='name'  label="Имя" errorMessage='Поле не заполненно' />
            <div className="input-group">
              <Input formSendClick={btnPressed} updateValid={updateValidateFormElements} indexInForm={2} type='text' inputType='email' label="Email" errorMessage='Поле не заполненно' />
              <Input formSendClick={btnPressed} updateValid={updateValidateFormElements} indexInForm={3} type='text' inputType='phone' label="Телефон" errorMessage='Поле не заполненно' />
            </div>
            <Textarea formSendClick={btnPressed} updateValid={updateValidateFormElements} indexInForm={4} placeholder='Оставьте пометку к заказу' errorMessage='Поле не заполненно' />
            <InputFile formSendClick={btnPressed} updateValid={updateValidateFormElements} indexInForm={5} errorMessage='Поле не заполненно' />
            <Checkbox formSendClick={btnPressed} updateValid={updateValidateFormElements} indexInForm={6} caption='Даю согласие на обработку своих персональных данных' errorMessage='Поле не заполненно' />
            <div className="button__wrapper">
              <Button type='btn-red' onClick={handleClickSend} >
                Оставить заявку
              </Button>
            </div>
          </div>
          }
          { validForm &&
            <div className='subscribe__form-part'>
              <h4>
                Мы получили вашу заявку. Наши специалисты свяжутся с вами в ближайшее время, чтобы уточнить все детали заказа.
              </h4>
              <div className="button__wrapper">
                <NavLink to='/'  className='link__navlink' >
                  <Button type='btn-red'onClick={handleClickSend} >
                    Вернуться на главную
                  </Button>
                </NavLink>
              </div>
            </div>
          }
          <div className="subscribe__info-part">
            <div className="info__content">
              <ul className="info__ul">
                <li className="info__item">
                  <p>Наша горячая линия</p>
                  <h2>8 800 38 23 112</h2>
                </li>
                <li className="info__item">
                  <p>Главный офис</p>
                  <h2>г. Москва, Садовническая ул., 80</h2>
                </li>
                <li className="info__item">
                  <p>Часы работы</p>
                  <h2>Пн-Пт с 10:00 до 22:00</h2>
                </li>
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>
  )
}
